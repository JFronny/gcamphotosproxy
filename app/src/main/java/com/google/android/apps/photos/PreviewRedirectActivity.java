package com.google.android.apps.photos;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class PreviewRedirectActivity extends Activity {
    private static final String ID = "PreviewRedirectActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(ID, getIntent().toString());

        if (getIntent().getPackage().equals(getPackageName())) {
            startActivity(new Intent(Intent.ACTION_VIEW, getIntent().getData()));
        }

        finish();
    }
}
